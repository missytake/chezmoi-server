# Setup

```
sudo apt update
sudo apt install -y git tmux curl
cd ~/.local
curl -sfL https://git.io/chezmoi | sh  # Install chezmoi
git clone https://git.0x90.space/missytake/chezmoi-server ~/.local/share/chezmoi  # clone the repository
bin/chezmoi diff
bin/chezmoi apply
source ~/.bash_profile
```

